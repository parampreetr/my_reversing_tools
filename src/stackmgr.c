#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<argp.h>

static struct argp_option options[] = {
	{ "find", 'f', "Query", 0, "Find string in a stack" },
	{ "check", 'c', "Variable Name", 0, "To check env payload for any bad chars" },
	{ "depth", 'd', "bytes", 0, "Depth of a stack for searching"}
};

static char args_doc[] = "ARG [Query...]";


static char doc[] = "\
\
stackmgr is developed to reduce some boring work while our target\
is to exploit a program by setting payload to environment variable\
and executing it though any other mean.\
\
\n\nFor example:\
\n--> To find shellcode env variable and its value.\
\n \tstackmgr find \"shellcode\"\
\n--> To print value of shellcode variable in bytes.\
\n \tstackmgr check \"shellcode\"";



struct arguments {
	int work;
	char *query;
	int depth;
};


static int parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;

	switch(key) {
		case 'f':
			arguments->work = 0;
			arguments->query = arg;
			break;
		case 'c':
			arguments->work = 1;
			arguments->query = arg;
			break;

		case 'd':
			arguments->depth = atoi(arg);
			break;

		default:
			return ARGP_ERR_UNKNOWN;
	}

	return 0;
}

static struct argp argp = {
	options,
	parse_opt,
	args_doc,
	doc
};


char *findstr(void *base, char *query, int depth) {

	char *result;
	int matched = 0, total_matches_found = 0;
	void *stack_addr;

	if(base == NULL){
		printf("[*] Getting space in stack\n");
		int tempvar;
		stack_addr = &tempvar;
		printf("[+] Got space at 0x%x\n", stack_addr);
	} else {
		stack_addr = base;
		printf("[*] Started searching from %x\n", stack_addr);
	}



	printf("[*] Digging down by 0x%x [%i] (till 0x%x)\n", depth, depth, stack_addr + depth);
	

	for(int i = 0; i < depth; i++) {
		stack_addr++;
		
		if(matched != 1) {
			total_matches_found = 0;
			result = (char *)stack_addr;
		}

		if(*(char *)stack_addr == query[total_matches_found]){
			matched = 1;
			total_matches_found++;
		} else {
			matched = 0;
		}

		if(total_matches_found == strlen(query)) {
			printf("Result fOunD: 0x%x\t\"", result);
			printf("%s", (char *)result);

			printf("\"\n");
			matched = 0;
			if(base != NULL)
				return result;
		}
	}

	return result;
}


void decode_val_of_var_at(char *target_addr) {
	char *value_addr, *initial_addr = target_addr, *backup_addr;
	int limit = 100;
	while(*target_addr != '\0') {
		limit--;
		target_addr++;

		if(*target_addr == '=') {
			value_addr = target_addr;
			break;
		}
	}

	if(*target_addr == '\0') {
		fprintf(stderr, "[!] Query found in environment value.\n");
		printf("%s\n", initial_addr);
	} else {
		printf("=\t--> %x\n", target_addr + 1);

	}

	limit = 100;
	backup_addr = target_addr;
	while(*initial_addr != '=' && *initial_addr != '\x00') {
		initial_addr++;
		limit--;
	}

	target_addr++;
	
	printf("\nValue shellcode: ");
	while(*target_addr != '\x00') {
		printf("\\x%x", *target_addr);
		target_addr++;
	}

	printf("\n");
}



int main(int argc, char* argv[]) {

	int depth, i, ret, length, matched = 0, j;
	char *result;

	struct arguments arguments;
	arguments.work = 0;
	arguments.query = "";
	arguments.depth = 4000;

	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	
	switch(arguments.work) {
		case 0:
			findstr(NULL, arguments.query, arguments.depth);
			break;

		case 1:
			result = (char *)&depth + 200;
			while(1 == 1) {
				printf("*---------------------------------------------------------------------------------------------------*\n");
				result = findstr(result, arguments.query, arguments.depth);
				decode_val_of_var_at(result);
				printf("*---------------------------------------------------------------------------------------------------*\n\n");
			}

		default:
			fprintf(stderr, "[-->] Use --help/-? for help message\n");
			return -1;
	}
}
